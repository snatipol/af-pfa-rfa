# AF-PFA-RFA

This project is the numerical implementation of three different mathematical models representing atrial fibrillation (AF), pulsed electric field ablation (PFA) and radiofrequency ablation (RFA). For more informations on the medical and mathematical theory beside, see [1]. Simulations obtained from these codes have been also illustrated in the article. The strategy is based on the FEM, implemented in the FreeFem++ software. Version 4.9 has been used for these tests. The code has not been tested with later versions.

Inside the folder `src` you will find three different codes and a mesh folder.

### FreeFem++ files
All Copyrights (c) of codes belong to: Simone Nati Poltri, 2023, Univ. Bordeaux, CNRS, Inria, Bordeaux INP, IMB, UMR 5251, F-33400 Talence, France.

>`AF_atrium.edp`: solves the AF problem, generating the so-called spirals. In the paper we set as re-entry time 356ms, carefully looking for the best time instant at the end of the repolarization wave. One could find better spirals, by changing the re-entry time and the shape and position of the re-entry source.

>`PFA_atrium.edp`: solves the PFA problem, considering two separate domains. A non overlapping Schwarz method has been numerically implemented, by setting the penalty parameter to 2. A tolerance has been set, in order to stop the sub iterations of the algorithm at a reasonable value.

>`RFA_atrium_vm_ue_weakcoupling.edp`: solves the RFA problem, considering two separate domains. A parameter alpha allows to consider different RFAs. The problem is solved for vm and ue through a weak coupling.

>`RFA_atrium_ui_ue_weakcoupling.edp`: solves the RFA problem, considering two separate domains. A parameter alpha allows to consider different RFAs. The problem is solved for ui and ue through a weak coupling.

>`RFA_atrium_ui_ue_schwarz.edp`: solves the RFA problem, considering two separate domains. A parameter alpha allows to consider different RFAs. The problem is solved for ui and ue through a non overlapping schwarz method.

### Mesh folder
All Copyrights (c) of meshes and fibers `.txt` files belong to: Annabelle Collin, 2023, Univ. Bordeaux, CNRS, Inria, Bordeaux INP, IMB, UMR 5251, F-33400 Talence, France.

The `mesh` folder contains two different sub-folders.
>`AF_mesh`: contains a surface mesh that describes the shape of a left atrium with pulmonary veins. Used in the code `AF_atrium.edp`.

>`LAPV_mesh`: contains two surface meshes describing the shape of a pulmonary vein and a left atrium with 3 pulmonary veins. It is nothing but the `AF_mesh`, cut into two meshes. Used in codes `PFA_atrium.edp`, `RFA_atrium_vm_ue_weakcoupling.edp`,
`RFA_atrium_ui_ue_weakcoupling.edp` and `RFA_atrium_ui_ue_schwarz.edp`.

---------------------------------------------------------------------------------------------------

Please cite the article [1] if you use the code in your project. 
Contact: annabelle.collin@inria.fr.

---------------------------------------------------------------------------------------------------
[1] S. Nati Poltri, G. Caluori, P. Jaïs, A. Collin, and C. Poignard. Electrocardiology modeling after catheter ablations for atrial fibrillation. In International Conference on Functional Imaging and Modeling of the Heart. Springer, 2023.
