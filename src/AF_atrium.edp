// --------------------------------------------------------------------------
// Copyright (c) Simone Nati Poltri, 2023, Univ. Bordeaux, CNRS, Inria, Bordeaux INP, IMB, UMR 5251, F-33400 Talence, France
// --------------------------------------------------------------------------

load "medit"
load "msh3"
load "iovtk"


cout << "==============================================\n";
cout << "  Bidomain model " << endl;
cout << "==============================================\n";

string root = "./";

// --------------------------------------------------------------------------
// TO RESTART - BEGIN
bool isrestart  			= false;
real timerestart 			= 330;
exec("mkdir "+root+"AF_restart");
// TO RESTART - END
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Read mesh
// --------------------------------------------------------------------------
string surfacefolder = "/mesh/AF_mesh";
string namemesh = "/left_atrium_only_surface_cm";
string resultsfolder = root+"AF_results";
meshS Th = readmeshS(root+surfacefolder+namemesh+".mesh");

// --------------------------------------------------------------------------
// Create results folder
// --------------------------------------------------------------------------

if (isrestart == false)
{
	exec("rm -rf " + resultsfolder);
}
exec("mkdir " + resultsfolder);

// --------------------------------------------------------------------------
// Model parameters
// --------------------------------------------------------------------------
real Am 					= 200;    // cm^(-1)
real Cm 					= 1e-3;   // mF.cm^(-2)

// Conductivities
// SIGMAi = sigmait I + (sigmail - sigmait) tau (x) tau
// SIGMAe = sigmaet I + (sigmael - sigmaet) tau (x) tau
real sigmait 				= 2.5e-4; // S.cm^(-1)
real sigmail 				= 2.5e-3; // S.cm^(-1)
real sigmaet 				= 9.0e-4; // S.cm^(-1)
real sigmael 				= 2.5e-3; // S.cm^(-1)

// Parameters of MitchellSchaeffer model
real tauin				 	= 4.0;    // cm^2.mA^(-1)
real tauout 				= 90.0;   // cm^2.mA^(-1)
real tauopen				= 300;    // ms
real tauclose				= 100;    // ms
real vmin					= -80;    // mV
real vmax					=  20;    // mV
real vgate					= -67;    // mV

// Bachmann's bundle
// First bundle
real vcenterBBx = 1.3;
real vcenterBBy = 1.6;
real vcenterBBz = -0.07;
real radiusBB = 0.4;
real longaxisBB = 2;
real smallaxisBB = 0.5;
real rotangell = -pi/5;
// Second bundle
real vcenterBBx2 = -0.14;
real vcenterBBy2 = 1.6;
real vcenterBBz2 = -0.07;
real radiusBB2 = 0.4;
real longaxisBB2 = 3.;
real smallaxisBB2 = 0.5;
real rotangell2 = 0; //-pi/8;

// Fossa Ovalis
real vcenterFOx = 2.6;
real vcenterFOy = 0.04;
real vcenterFOz = -0.015;
real radiusFO = 0.1;
real timearrivalFO = 10;

// Reentry
bool reentry = 1;
real vcenterREx = -1.38;
real vcenterREy = -0.93;
real vcenterREz = 1.41;
real radiusRE = 0.4;
real a(0.1), b(0.2), c(0.3);
real timereentry = 356;

// --------------------------------------------------------------------------
// Read fibers
// --------------------------------------------------------------------------
ifstream fibersFile(root+surfacefolder+"/left_atrium_only_surface_fibers_mean.txt");
ifstream fibersFileOrtho(root+surfacefolder+"/left_atrium_only_surface_fibers_ortho.txt");
ifstream anglesFile (root+surfacefolder+"/left_atrium_only_surface_angles.txt");
real[int,int] fibers(Th.nv,3);
real[int,int] fibersOrtho(Th.nv,3);
real[int] angles(Th.nv);
for (int i=0; i < Th.nv; i++)
{
	anglesFile >> angles(i);
	for (int j=0; j < 3; j++)
	{
		fibersFile >> fibers(i,j);
		fibersFileOrtho >> fibersOrtho(i,j);
	}
}

// --------------------------------------------------------------------------
// Discrete Space of Adminissible Solution
// --------------------------------------------------------------------------
fespace Vh(Th,P1);
Vh vm, vmNew, phi; // Functions
Vh ue, psi;
Vh taux, tauy, tauz;
Vh tauorthox, tauorthoy, tauorthoz;
Vh Iangle;
Vh w;

Vh ElectroporatedArea = 0.; // "Boolean" of the area


for (int i = 0; i < Vh.ndof; i++)
  {
    taux[][i] = fibers(i,0);
    tauy[][i] = fibers(i,1);
    tauz[][i] = fibers(i,2);
    tauorthox[][i] = fibersOrtho(i,0);
    tauorthoy[][i] = fibersOrtho(i,1);
    tauorthoz[][i] = fibersOrtho(i,2);
    if (abs(angles(i))<1e-7)
      {
	Iangle[][i] = 1.0;
      }
    else
      {
	Iangle[][i] = 0.5+0.25/angles(i)*sin(2*angles(i));
      }
  }


// --------------------------------------------------------------------------
// Time constants
// --------------------------------------------------------------------------
real time = 0;	// Time variable
real dt = 0.01; // Time step
real tfinal = 700; // Final time
int freqSolveIt = 100;

// --------------------------------------------------------------------------
// Initialization
// --------------------------------------------------------------------------
vm = vmin + (vmax-vmin)*(( ((x-vcenterBBx)*cos(rotangell)+(z-vcenterBBz)*sin(rotangell))^2/longaxisBB^2
						   +(y-vcenterBBy)^2
						 +(-(x-vcenterBBx)*sin(rotangell)+(z-vcenterBBz)*cos(rotangell))^2/smallaxisBB^2) < radiusBB)
		  + (vmax-vmin)*(( ((x-vcenterBBx2)*cos(rotangell2)+(z-vcenterBBz2)*sin(rotangell2))^2/longaxisBB2^2
		  				   +(y-vcenterBBy2)^2
						   +(-(x-vcenterBBx2)*sin(rotangell2)+(z-vcenterBBz2)*cos(rotangell2))^2/smallaxisBB2^2) < radiusBB2);
vm = vm*(vm<=vmax) + vmax*(vm>vmax);
vmNew = vm; ue = 0;
w = 1/((vmax-vmin)*(vmax-vmin));

// --------------------------------------------------------------------------
// Macro
// --------------------------------------------------------------------------
macro grad(u) [dx(u),dy(u),dz(u)] //EOM
macro taucrosstaugrad(u,f0,f1,f2) [f0*f0*dx(u)+f0*f1*dy(u)+f0*f2*dz(u),f0*f1*dx(u)+f1*f1*dy(u)+f1*f2*dz(u),f0*f2*dx(u)+f1*f2*dy(u)+f2*f2*dz(u)] //EOM

// --------------------------------------------------------------------------
// Variational formulation
// --------------------------------------------------------------------------

problem transmembranePot([vmNew,ue],[phi,psi], init=1, solver=GMRES) =
// bilinear form
  int2d(Th)   ( Am*Cm*vmNew/dt*phi )

  + int2d(Th) ( (sigmait*grad(vmNew))'*grad(phi) )
  + int2d(Th) ( ((sigmail-sigmait)*Iangle*taucrosstaugrad(vmNew,taux,tauy,tauz))'*grad(phi) )
  + int2d(Th) ( ((sigmail-sigmait)*(1.0-Iangle)*taucrosstaugrad(vmNew,tauorthox,tauorthoy,tauorthoz))'*grad(phi) )
  + int2d(Th) ( (sigmait*grad(ue))'*grad(phi) )
  + int2d(Th) ( ((sigmail-sigmait)*Iangle*taucrosstaugrad(ue,taux,tauy,tauz))'*grad(phi) )
  + int2d(Th) ( ((sigmail-sigmait)*(1.0-Iangle)*taucrosstaugrad(ue,tauorthox,tauorthoy,tauorthoz))'*grad(phi) )
  + int2d(Th) ( (sigmait*grad(vmNew))'*grad(psi) )
  + int2d(Th) ( ((sigmail-sigmait)*Iangle*taucrosstaugrad(vmNew,taux,tauy,tauz))'*grad(psi) )
  + int2d(Th) ( ((sigmail-sigmait)*(1.0-Iangle)*taucrosstaugrad(vmNew,tauorthox,tauorthoy,tauorthoz))'*grad(psi) )
  + int2d(Th) ( ((sigmait+sigmaet)*grad(ue))'*grad(psi) )
  + int2d(Th) ( (((sigmail-sigmait)+(sigmael-sigmaet))*Iangle*taucrosstaugrad(ue,taux,tauy,tauz))'*grad(psi) )
  + int2d(Th) ( (((sigmail-sigmait)+(sigmael-sigmaet))*(1.0-Iangle)*taucrosstaugrad(ue,tauorthox,tauorthoy,tauorthoz))'*grad(psi) )

  // euler
  - int2d(Th) ( Am*Cm*vm/dt*phi )
  // reaction
  - int2d(Th) ( ( Am*(w*(vm-vmin)*(vm-vmin)*(vmax-vm)/(tauin*(vmax-vmin))-(vm-vmin)/(tauout*(vmax-vmin))) )*phi )

  ;

// --------------------------------------------------------------------------
// Time Loop
// --------------------------------------------------------------------------
int Nit = 0;
int NitSave = 0;

if (isrestart == true)
{
	// Read file
	ifstream filevm(root+"/AF_restart/left_atrium_vm_init_surf_restart_"+timerestart+".txt");
	ifstream fileue(root+"/AF_restart/left_atrium_ue_init_surf_restart_"+timerestart+".txt");
    ifstream filew(root+"/AF_restart/left_atrium_w_init_surf_restart_"+timerestart+".txt");

	real[int] vminit(Th.nv);
	real[int] ueinit(Th.nv);
    real[int] winit(Th.nv);

	for (int i=0; i < Th.nv; i++)
	{
		filevm >> vminit(i);
		fileue >> ueinit(i);
        filew >> winit(i);
	}
	for (int i = 0; i < Vh.ndof; i++)
	{
	  vm[][i] = vminit(i);
	  ue[][i] = ueinit(i);
      w[][i] = winit(i);
	}
	// Update time
	time = timerestart;
	Nit = timerestart/dt;
	NitSave = timerestart;
}

while (1)
  {
    string resultsvtkvm = "./"+resultsfolder+"/left_atrium_vm_"+NitSave+".vtk";
    string resultsvtkue = "./"+resultsfolder+"/left_atrium_ue_"+NitSave+".vtk";

    if (Nit % freqSolveIt == 0)
      {
	savevtk(resultsvtkvm,Th,vm);
	savevtk(resultsvtkue,Th,ue);
	NitSave += 1;
      }

    if (time > tfinal-dt/2.) break;

    cout << "--------------------------------------------\n";
    cout << "  Iteration of Time " << time << endl;
    cout << "--------------------------------------------\n";

    if (isrestart == false)
      {
	if ((time > timerestart-dt/2.0) && (time < timerestart+dt/2.0))
	  {
	    cout << "Save in order to restart at time " << timerestart << endl;
	    ofstream resultsvtkvminit(root+"/AF_restart/left_atrium_vm_init_surf_restart_"+timerestart+".txt");
	    ofstream resultsvtkueinit(root+"/AF_restart/left_atrium_ue_init_surf_restart_"+timerestart+".txt");
        ofstream resultsvtkwinit(root+"/AF_restart/left_atrium_w_init_surf_restart_"+timerestart+".txt");

	    for (int i = 0; i < Vh.ndof; i++)
	      {
		resultsvtkvminit << vm[][i] << endl;
		resultsvtkueinit << ue[][i] << endl;
        resultsvtkwinit << w[][i] << endl;
	      }
	  }
      }


    // -----------------
    // Fossa ovalis
    // -----------------
    if ((time > timearrivalFO) && (time < timearrivalFO + 2*dt))
      {
	vm = vm*(((x-vcenterFOx)^2.+(y-vcenterFOy)^2.+(z-vcenterFOz)^2.) >= radiusFO)+vmax*(((x-vcenterFOx)^2.+(y-vcenterFOy)^2.+(z-vcenterFOz)^2.) < radiusFO);
      }

    // ----------------
    // Reentry
    // ----------------

    if ((reentry==true) && (time > timereentry) && (time < timereentry + 18*dt))
      {
	vm = vm + (vmax-vm)*(( (x-vcenterREx)^2./a+(y-vcenterREy)^2./b
			       + (z-vcenterREz)^2./c) < radiusRE ); // An ellipsoid
	// vm = vm + (vmax-vm)*(( abs(x-vcenterREx) + abs(y-vcenterREy)) < radiusRE) * (abs(z-vcenterREz) < 0.1); // A parallelogram
      }


    // -----------------
    // Following cardiac cycles (every 700ms or so)
    // -----------------
    if (time%700<dt) // Bachmann's bundles
      {
	vm = vm + (vmax-vm)*(( ((x-vcenterBBx)*cos(rotangell)+(z-vcenterBBz)*sin(rotangell))^2/longaxisBB^2
			       +(y-vcenterBBy)^2
			       +(-(x-vcenterBBx)*sin(rotangell)+(z-vcenterBBz)*cos(rotangell))^2/smallaxisBB^2) < radiusBB)
	  + (vmax-vm)*(( ((x-vcenterBBx2)*cos(rotangell2)+(z-vcenterBBz2)*sin(rotangell2))^2/longaxisBB2^2
			 +(y-vcenterBBy2)^2
			 +(-(x-vcenterBBx2)*sin(rotangell2)+(z-vcenterBBz2)*cos(rotangell2))^2/smallaxisBB2^2) < radiusBB2);
	vm = vm*(vm<=vmax) + vmax*(vm>vmax);
      }
    if ( (time%700 > timearrivalFO) && (time%700 < timearrivalFO + dt) ) // Fossa ovalis
      {
	vm = vm*(((x-vcenterFOx)^2.+(y-vcenterFOy)^2.+(z-vcenterFOz)^2.) >= radiusFO)+vmax*(((x-vcenterFOx)^2.+(y-vcenterFOy)^2.+(z-vcenterFOz)^2.) < radiusFO);
      }

    // -----------------
    // Solve the problem
    // -----------------
    w = w + dt*((1.0/((vmax-vmin)*(vmax-vmin))-w)*(vm < vgate)/tauopen-w*(vm > vgate)/tauclose);

    transmembranePot;
    real meanue = (int2d(Th) (ue)) / (int2d(Th) (1.0));
    cout << "Mean of ue : " << meanue << endl;
    ue = ue - meanue;
    vm = vmNew;

    // -----------------
    // Update
    // -----------------
    time += dt;
    Nit += 1;
 }
